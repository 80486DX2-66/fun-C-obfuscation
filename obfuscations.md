# Obfuscations

1. `#define macro(x, y) ( x ## y )`: Concatenates literals `x` and `y` into a literal
2. `??!` (trigraph) means the same as `|`
   > **Note**
   > Trigraphs will be removed in the ISO C23 standard.

