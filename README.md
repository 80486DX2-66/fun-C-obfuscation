# fun-C-obfuscation
A collection of obfuscated for fun C code written mostly by me.

The primary goal is to shrink existing C code that I have written. However, some of the code will only work as intended on little-endian machines.
