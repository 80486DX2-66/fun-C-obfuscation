#!/bin/sh

src="$1"
output="./${src%.*}"
CC=${CC:-"gcc"}
CFLAGS=${CFLAGS:-"-Ofast -march=native -mtune=native"}
CFLAGS_SUFFIX=${CFLAGS_SUFFIX:-"-fdiagnostics-color"}
log_file="./compilation-output.log"

run_test() {
	"$output" "$@"
	rm "$output" > /dev/null 2>&1
}

view_log() {
	less -R $log_file
}

rm $log_file 2> /dev/null
shift
$CC $CFLAGS -o $output $src $CFLAGS_SUFFIX 2> $log_file && run_test "$@" \
	|| view_log
