/*
 * A generator of the 8000 Hz bytebeat for 240 seconds = 4 minutes:
 * (42&t>>9)*(t>>6|t|t>>(t>>16))+(7&t>>11)
 */

/*
 * Machine dependencies:
 * 1. Little-endian
 */

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#define FORMULA(t)((42&t>>9)*(t>>6|t|t>>(t>>16))+(7&t>>11))
#define Z uint8_t
#define Y uint16_t
#define I uint32_t
#define G(y,x,z){y x=z;fwrite(&x,sizeof(x),1,r);}
#define J fwrite(&t,D,1,r)
I V=8000,M=240;int main(){size_t C=sizeof(Z);size_t B=sizeof(Y);size_t D=sizeof(I);int L=V*M;Z*q=calloc(L,C);if(!q)return 1;for(uint64_t w=0;w<L;w++)q[w]=(Z)(FORMULA(w))&255;FILE*r=fopen("output.wav","wb");if(!r)return 1;fwrite("RIFF",1,4,r);I t=L;J;fwrite("WAVE",1,4,r);fwrite("fmt ",1,4,r);G(I,y,16);G(Y,u,1);G(Y,i,1);G(I,o,V);G(I,s,V);G(Y,p,1);G(Y,a,8);fwrite("data",1,4,r);J;fwrite(q,C,L,r);fclose(r);free(q);return 0;}
