/*
 * Machine dependencies:
 * 1. CPU has instruction 'CPUID' (likely x86 and x86_64 only)
 */

#include<cpuid.h>
#include<string.h>
#define U(Z) Z?"yes":"no"
G,A,B,C,D;Q(){return __get_cpuid(1,&A,&B,&C,&D)?C&32:0;}W(){return __get_cpuid(1+2*G,&A,&B,&C,&D)?C&4:0;}R(){return __get_cpuid(1,&A,&B,&C,&D)?C&2*G:0;}T(){__cpuid_count(G,0,A,B,C,D);return A>=1+G&&B==0x4b4d564b&&C==0x564b4d56&&!D;}Y(){return __get_cpuid(1+2*G,&A,&B,&C,&D)?D&(1<<20):0;}main(){G=4<<28;char V[13]={0};__asm("mov $0,%%eax;cpuid;mov %%ebx,%0;mov %%edx,%1;mov %%ecx,%2;":"=m"(V[0]),"=m"(V[4]),"=m"(V[8]));printf("VT-x: %s\nIs AMD CPU, AMD-V: %s, %s\nHyper-V: %s\nKVM: %s\nPAE-NX: %s\n",U(Q()),U(!strcmp(V,"AuthenticAMD")),U(W()),U(R()),U(T()),U(Y()));}
