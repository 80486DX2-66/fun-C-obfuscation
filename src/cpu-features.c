/*
 * Machine dependencies:
 * 1. CPU has instruction 'CPUID' (likely x86 and x86_64 only)
 */

#include<cpuid.h>
#include<string.h>
#define c(x)__get_cpuid(x,&A,&B,&C,&D)
#define U(Z)Z?"yes":"no"
G,A,B,C,D;Q(){return c(1)?C&32:0;}W(){return c(1+2*G)?C&4:0;}R(){return c(1)?C&2*G:0;}T(){__cpuid_count(G,0,A,B,C,D);return A>=1+G&&B==0x4b4d564b&&C==0x564b4d56&&!D;}Y(){return c(1+2*G)?D&(1<<20):0;}main(){G=4<<28;char V[13];V[12]=0;__asm("mov $0,%%eax;cpuid;mov %%ebx,%0;mov %%edx,%1;mov %%ecx,%2;":"=m"(V),"=m"(V[4]),"=m"(V[8]));printf("Vendor information: %s\nVT-x: %s\nIs AMD CPU, AMD-V: %s, %s\nHyper-V: %s\nKVM: %s\nPAE-NX: %s\n",V,U(Q()),U(!strcmp(V,"AuthenticAMD")),U(W()),U(R()),U(T()),U(Y()));}
