/*
 * obfuscated-atoi.h
 *
 * Author: Intel A80486DX2-66
 * License: Creative Commons Zero 1.0 Universal or Unlicense
 * SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

int atoi(const char*s);
