/*
 * Machine dependencies:
 * 1. Little-endian
 * 2. sizeof(char) = 1 byte
 */

#include <stdio.h>

/*
 * This program depends on a little-endian machine, i.e. how an `int`
 * variable is stored in memory.
 *
 * See file `first-byte-pointer.demo.c` for a demonstration how this work.
 */

int main(){printf("%d\n",*(char*)&*(int[]){5555555});}
