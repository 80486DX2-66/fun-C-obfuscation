/*
 * Original source code by FidgetSpinzz
 *
 * Machine dependencies:
 * 1. Little-endian
 * 2. 1 byte = 8 bits
 * 3. CPU can handle 64-bit values
 */

#include <stdio.h>
int main()
{
    long long int sex = 0x000064656e6e6162;
    printf((char*)&sex);
    return 0;
}
