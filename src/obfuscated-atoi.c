/*
 * obfuscated-atoi.c
 *
 * The original code is licensed to and was taken from musl libc, originally
 * stored at src/stdlib/atoi.c
 *
 * musl libc is licensed under MIT license:
 *     musl as a whole is licensed under the following standard MIT license:
 *
 *     ----------------------------------------------------------------------
 *     Copyright © 2005-2020 Rich Felker, et al.
 *
 *     Permission is hereby granted, free of charge, to any person obtaining
 *     a copy of this software and associated documentation files (the
 *     "Software"), to deal in the Software without restriction, including
 *     without limitation the rights to use, copy, modify, merge, publish,
 *     distribute, sublicense, and/or sell copies of the Software, and to
 *     permit persons to whom the Software is furnished to do so, subject to
 *     the following conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *     IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *     CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *     TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The license information of this modification:
 *     Author: Intel A80486DX2-66
 *     License: Creative Commons Zero 1.0 Universal or Unlicense
 *     SPDX-License-Identifier: CC0-1.0 OR Unlicense
 */

/*
 * Machine dependencies: no specific dependencies have been identified
 */

int atoi(const char*s){int n=0,neg;while(*s==32)s++;neg=*s-45?*s-43?0:!s++:!!s++;while(*s>47&&*s<58)n=10*n-(*s++-48);return neg?n:-n;}
