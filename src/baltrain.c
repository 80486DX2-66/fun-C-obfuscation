/*
 * L E G A L   N O T I C E: This program includes certain phrases and meanings
 * from Baldi's Basics (by Basically Games) for the purpose of displaying them
 * for fun. It is important to note that this program does not seek to profit
 * from the usage of these elements and is released under the Creative Commons
 * Zero 1.0 Universal license.
 *
 * U S E R   N O T I C E: Please note that some of these phrases may be
 * considered offensive or inappropriate to sensitive individuals. User
 * discretion is advised.
 */

/*
 * Machine dependencies:
 * 1. OS is Microsoft Windows, or a Unix/BSD system with `timespec` structure,
 * function `int nanosleep(const struct timespec *req, struct timespec
 * *_Nullable rem)`, and `errno` are available
 */

#include<stdio.h>
#include<stdlib.h>
#ifdef _WIN32
#include<windows.h>
#else
#include<errno.h>
Sleep(x){struct timespec a,b;a.tv_sec=x/1E3;a.tv_nsec=(x%1000)*1E6;while(nanosleep(&a,&b)==-1&&errno==EINTR)a=b;}
#endif
O,R,T,a,b,i,m,n,x,y;P(_){return rand()%_;}V(){return P(6)==4&&T++>4;}p(d){switch(d){case 0:a=P(10);b=P(10);m=P(2);break;case 1:a=P(51);b=P(51);m=P(2);break;case 2:b=1+P(10);a=b*(1+P(10));m=2+P(2);}switch(m){case 0:y=a+b;n='+';break;case 1:y=a-b;n='-';break;case 2:y=a*b;n='*';break;case 3:y=a/b;n='/';break;}O=y;if(V()){switch(m){case 0:y=a-b;break;case 1:y=a+b;break;case 2:y=a+b;break;case 3:y=a-b;break;}R=O!=y;}printf("%d %c %d = ",a,n,b);for(i=(1046-P(716))/10;i>0;i--){Sleep(10);}if(V()){y+=P(23)-11;R=O!=y;}printf("%d\n",y);u(R);return R;}u(x){printf("- %s\n",x?(char*[]){"You're bad at math!","I hear every door you open!",">:(","*slap*"}[P(4)]:(char*[]){"Aha! You got it!","Great job! That's right!","Good one!","You're doing fantastic!","I can't believe it! You're incredible!","Woah, I think you might be smarter than me!","Wow!"}[P(7)]);}main(){srand(time(NULL));printf("Baldi's Basics in Education and Learning: an unofficial training program!\nLicensed under Unlicense license.\n\n");for(i=0;!fflush(stdout);i=++i%3)if(p(i))break;}
