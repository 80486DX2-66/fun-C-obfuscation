/*
 * A generator of the 8000 Hz bytebeat for 240 seconds = 4 minutes:
 * (42&t>>9)*(t>>6|t|t>>(t>>16))+(7&t>>11)
 */

/*
 * Machine dependencies:
 * 1. Little-endian
 */

#include<stdio.h>
#include<stdlib.h>
#define Z char
#define Y short
#define I unsigned
#define G(y,x,z){y x=z;fwrite(&x,sizeof x,1,r);}
#define J fwrite(&L,4,1,r)
I V=8000,M=240;main(){I L=V*M;Z*q=malloc(L);if(!q)exit(1);for(I w=0;w<L;w++)q[w]=((42&w>>9)*(w>>6|w|w>>(w>>16))+(7&w>>11))&255;FILE*r=fopen("output.wav","wb");if(!r)exit(1);fwrite("RIFF",1,4,r);J;fwrite("WAVE",1,4,r);fwrite("fmt ",1,4,r);G(I,y,16);G(Y,u,1);G(Y,i,1);G(I,o,V);G(I,s,V);G(Y,p,1);G(Y,a,8);fwrite("data",1,4,r);J;fwrite(q,1,L,r);fclose(r);free(q);exit(0);}
