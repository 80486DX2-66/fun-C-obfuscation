/*
 * Machine dependencies:
 * 1. Little-endian
 * 2. 1 byte = 8 bits
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

int main(void) {
	uint32_t value = 5555555; /* 4 byte value */
	uint8_t bytes[4];

	bytes[0] = value         & 0xFF; /* get byte #1 */
	bytes[1] = (value >> 8)  & 0xFF; /* get byte #2 */
	bytes[2] = (value >> 16) & 0xFF; /* get byte #3 */
	bytes[3] = (value >> 24) & 0xFF; /* get byte #4 */

	for (size_t i = 0; i < 4; i++)
		printf("Byte #%zu = %" PRIu8 "\n", i + 1, bytes[i]);
		// // if the line above outputs %zu as it is, substitute it to the
		// // line below (and uncomment)
		// printf("Byte #%u = %" PRIu8 "\n", (unsigned)(i + 1), bytes[i]);

	/*
	 * A signed integer 5555555 is stored as 0x63c55400 on little-endian
	 * systems. We've represented it as separate bytes 0x63, 0xc5, 0x54,
	 * 0x00. The first one is 0x63 is 99 in decimal.
	 *
	 * `*(char*)&x` gets the first character of `x` (functionally, a byte).
	 *
	 * In the original program we create an anonymous character array, in
	 * which we store the value.
	 *     (int[]){5555555}
	 * `*(int[]){x}` makes compiler copy `x` to memory so we could make a
	 * pointer to `x`: `&*(int[]){x}`.
	 * This is done only to obfuscate code. We also could write it as:
	 *     int x=5555555;
	 */

	return 0;
}
