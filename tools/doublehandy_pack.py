#!/usr/bin/env python3

# Thanks to ilyakurdyukov for the original program
# URL: https://github.com/ilyakurdyukov/ioccc/blob/main/practice/2022.09-doublehandy/prog.c

import struct
from sys import argv

code = open(argv[1], "rb").read()

"""
Check that the code length is a multiple of 8 (since we are using 
doubles on x86
"""
if len(code) % 8 != 0:
    print("Code length must be a multiple of 8 bytes")
    raise SystemExit

doubles = []

"""
Iterate over the code, taking 8 bytes at a time and converting them to doubles
"""
for i in range(0, len(code), 8):
    double_value = struct.unpack("d", code[i:i+8])[0]
    doubles.append(double_value)

"""
Insert the values into the template
"""
res = """\
#if !defined(__i386__) && !defined(__x86_64__)
#error not that handy
#endif

#include <signal.h>

const __attribute__((section(".text.")))

double handy[] = {
\t%s
};

int main() {
\t*(double volatile*)handy *= (long) //* distraction *//;
\tsignal(SIGSEGV, (void*)handy);
}
""" % (
    ",\n\t".join(["%1.16g" % x for x in doubles])
)

print(res)
