#!/usr/bin/env python3

# C_code_chars_count.py
#
# Author: Intel A80486DX2-66
# License: Creative Commons Zero 1.0 Universal or Unlicense
# SPDX-License-Identifier: CC0-1.0 OR Unlicense

from os.path import basename
from sys import argv
import re

comment_stripping_regex = \
	re.compile(r"(?:\/\*(?:[^*]|[\n]|(?:\*+(?:[^*\/]|\n])))*\*+\/)|(?:\/\/.*)",
	           re.MULTILINE | re.DOTALL)


def count_code_size(lines, **strip):
	if not strip:
		strip["comments"] = True
		strip["empty_lines"] = True

	old_size = sum(len(line) for line in lines)

	if strip["empty_lines"]:
		lines = [line for line in lines if line.strip()]
	if strip["comments"]:
		lines = re.sub(comment_stripping_regex, "", "\n".join(lines)) \
				.replace("\n", "")

	new_size = sum(len(line) for line in lines)
	return [new_size, old_size]


if __name__ == "__main__":
	if len(argv) < 2:
		print(f"Usage: {basename(argv[0])} <source code file path>")
		exit(1)

	with open(argv[1], "r", encoding="utf-8-sig") as file:
		code_chars, all_chars = count_code_size(file.read().splitlines())

	percent_of_code = (code_chars * 100) / all_chars
	display_percent_of_code = "(%.2f%%)" % percent_of_code
	print("Code characters:", code_chars, display_percent_of_code)

	percent_of_coms = ((all_chars - code_chars) * 100) / all_chars
	display_percent_of_coms = "(%.2f%% of comments)" % percent_of_coms
	print("All characters:", all_chars, display_percent_of_coms)
